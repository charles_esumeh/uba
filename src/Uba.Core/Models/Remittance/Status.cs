﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models
{
    public class Status
    {
        public string transactionId { get; set; }
        public string hsTransactionId { get; set; }
        public DateTime maxCompletionDate { get; set; } = DateTime.Now;
        public string state { get; set; }
        public string stateLabel { get; set; }
        public VendorSpecific vendorSpecificFields { get; set; }
    }
}
