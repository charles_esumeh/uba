﻿using System;
using System.Runtime.Serialization;
using Uba.Core.Models;

namespace Uba.Core
{
    [DataContract]
    public class AccountRequest : StatusRequest
    {
        [DataMember(Name = "sourceUri")]
        public string sourceUri { get; set; }
        [DataMember(Name = "destinationUri")]
        public string destinationUri { get; set; }
        [DataMember(Name = "ChannelCode")]
        public string ChannelCode { get; set; }
        [DataMember(Name = "Stan")]
        public string Stan { get; set; }
        [DataMember(Name = "amountInformation")]
        public AmountInformation amountInformation { get; set; }
        [DataMember(Name = "senderInformation")]
        public SenderInformation senderInformation { get; set; }
        [DataMember(Name = "description")]
        public string description { get; set; }
        [DataMember(Name = "provisionalResponseSupport")]
        public ProvisionalResponseSupport provisionalResponseSupport { get; set; }

    }
}
