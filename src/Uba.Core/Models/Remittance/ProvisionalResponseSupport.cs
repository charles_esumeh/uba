﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core
{
    public class ProvisionalResponseSupport
    {
        public string reportStatusTo { get; set; }
        public string token { get; set; }
    }
}
