﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Uba.Core.Models
{
    public enum RoutingTags
    {
        [Description("UBAGRP-BE-BANK")]
        RouteBE,
        [Description("UBAGRP -BEN-BANK")]
        RouteBEN,
        [Description("UBAGRP-BF-BANK")]
        RouteBF,
        [Description("UBAGRP-BFA-BANK")]
        RouteBFA,
        [Description("UBAGRP-CI-BANK")]
        RouteCI,
        [Description("UBAGRP-CIA-BANK")]
        RouteCIA,
        [Description("UBAGRP-CM-BANK")]
        RouteCM,
        [Description("UBAGRP-GH-BANK")]
        RouteGH,
        [Description("UBAGRP-GHA-BANK")]
        RouteGHA,
        [Description("UBAGRP-LR-BANK")]
        RouteLR,
        [Description("UBAGRP-SL-BANK")]
        RouteSL,
        [Description("UBAGRP-SLL-BANK")]
        RouteSLL,
        [Description("UBARCV-GH-BANK")]
        RouteCVGH,
        [Description("UBARCV-NG-BANK")]
        RouteCVNG
        
    }

    public static class MyEnumExtensions
    {
        public static string ToDescriptionString(this RoutingTags val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
