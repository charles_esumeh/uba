﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models
{
    public class StatusError
    {
        public int id { get; set; }
        public string name { get; set; }
        public string typeName { get; set; }
    }
}
