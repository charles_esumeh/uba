﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Uba.Core.Models;

namespace Uba.Core
{
    public class AccountResponse : ErrorStatus
    {
        public string hsTransactionId { get; set; }
        public string UBATransactionId { get; set; }
        public AccountInformation accountInformation { get; set; }
    }
}
