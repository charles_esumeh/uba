﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Uba.Core
{
    [DataContract]
    public class AccountInformation
    {
        [DataMember(Name = "responseMessage")]
        public string responseMessage { get; set; }
        [DataMember(Name = "balanceCurrency")]
        public string balanceCurrency { get; set; }
        //[DataMember(Name = "State")]
        //public string State { get; set; }
        [DataMember(Name = "responseCode")]
        public string responseCode { get; set; }
        [DataMember(Name = "accountName")]
        public string accountName { get; set; }
    }
}
