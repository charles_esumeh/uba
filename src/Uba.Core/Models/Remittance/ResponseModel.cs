﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Models
{
    public class ResponseModel
    {
        //[AutoWrapperPropertyMap(Prop.IsError)]
        public bool RequestSuccessful { get; set; }
        //[AutoWrapperPropertyMap(Prop.Result)]
        public object ResponseData { get; set; }
        public string Message { get; set; }
        //[AutoWrapperPropertyMap(Prop.StatusCode)]
        public string ResponseCode { get; set; }
    }
}
