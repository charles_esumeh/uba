﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core
{
    public class ProvisionalResponse
    {
        public DateTime maxCompletionDate { get; set; } = DateTime.Now;
        public string state { get; set; }
        public string label { get; set; }
    }
}
