﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core
{
    public class SenderInformation
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string SourceIban { get; set; }
        public string SourceCode { get; set; }
    }
}
