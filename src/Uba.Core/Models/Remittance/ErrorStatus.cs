﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models
{
    public class ErrorStatus
    {
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}
