﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core
{
    public class TransactionRequest
    {
        public long Id { get; set; }
        public string ClientId { get; set; }
        public string login { get; set; }
        public string Password { get; set; }
        public string transactionId { get; set; }
        public string UBATransactionId { get; set; }
        public string hstransactionId { get; set; }
        public string sourceUri { get; set; }
        public string destinationUri { get; set; }
        public string Stan { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string grace { get; set; }
        public string validity { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SourceIban { get; set; }
        public string SourceCode { get; set; }
        public string routingTag { get; set; }
        public string description { get; set; }
        public string ChannelCode { get; set; }
        public string fieldId { get; set; }
        public string value { get; set; }
        public string BeneficiaryIban { get; set; }
        public string BenefName { get; set; }
        public string BenefBank { get; set; }
        public string BenefBranch { get; set; }
        public string BenefSortCode { get; set; }
        public string BenefCode { get; set; }
        public string reportStatusTo { get; set; }
        public string token { get; set; }
        public string ClientName { get; set; }

    }
}


