﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Uba.Core
{
    [DataContract]
    public class AmountInformation
    {
        [DataMember(Name = "currency")]
        public string currency { get; set; }
        [DataMember(Name = "amount")]
        public string amount { get; set; }
        [DataMember(Name = "grace")]
        public string grace { get; set; }
        [DataMember(Name = "validity")]
        public string validity { get; set; }
    }
}
