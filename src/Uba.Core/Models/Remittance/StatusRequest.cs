﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Uba.Core.Models
{
    [DataContract]
    public class StatusRequest
    {
        [DataMember(Name = "Security")]
        public Security Security { get; set; }
        [DataMember(Name = "hstransactionId")]
        public string hstransactionId { get; set; }
        [DataMember(Name = "transactionId")]
        public string transactionId { get; set; }
        [DataMember(Name = "routingTag")]
        public string routingTag { get; set; }
        [DataMember(Name = "vendorSpecificFields")]
        public VendorSpecific vendorSpecificFields { get; set; }
    }
}
