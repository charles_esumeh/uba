﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core
{
    public class VendorSpecific
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string fieldId { get; set; }
        public string value { get; set; }
        public string BenefCode { get; set; }
        public string BeneficiaryIban { get; set; }
        public string BenefSortCode { get; set; }
        public string BenefBranch { get; set; }
        public string BenefBank { get; set; }
        public string BenefName { get; set; }
    }
}
