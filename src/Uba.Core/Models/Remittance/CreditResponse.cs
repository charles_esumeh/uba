﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Uba.Core.Models;

namespace Uba.Core
{
    public class CreditResponse : ErrorStatus
    {
        public string transactionId { get; set; }
        public string UBATransactionId { get; set; }
        public string hsTransactionId { get; set; }
        public VendorSpecific vendorSpecificFields{ get; set; }
        public ProvisionalResponse provisionalResponse { get; set; }
    }
}
