﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class TxnListResponse
    {
        public string code { get; set; }
        public string status { get; set; }
        public TransactionListModel transaction { get; set; }
    }
}
