﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class ListTransactions
    {
        [Required]
        public string PERIOD_FIRST { get; set; }
        [Required]
        public string PERIOD_LAST { get; set; }

    }
}
