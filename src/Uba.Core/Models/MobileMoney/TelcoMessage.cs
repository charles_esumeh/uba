﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models.MobileMoney
{
    public class TelcoMessage
    {
        public string smessage { get; set; }
        public string telcos { get; set; }
        public string currency { get; set; }
    }
}
