﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class WalletRequest
    {
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string Provider { get; set; }
        [Required]
        public string CountryCode { get; set; }
        public string TelSecurityCode { get; set; }
    }
}
