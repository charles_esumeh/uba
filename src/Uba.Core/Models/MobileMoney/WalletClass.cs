﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class WalletClass
    {
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string Provider { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string DebitAccount { get; set; }
        [Required]
        public string CurrencyCode { get; set; }
        [Required]
        public string AccountType { get; set; }
        [Required]
        public string RequestID { get; set; }
    }
}
