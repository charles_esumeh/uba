﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class TxnStatusRequest
    {
        [Required]
        public string RequesterID { get; set; }
        [Required]
        public string Provider { get; set; }
        [Required]
        public string CountryCode { get; set; }
    }

}
