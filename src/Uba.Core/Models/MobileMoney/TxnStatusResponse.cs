﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class TxnStatusResponse
    {
        public MessageModel message { get; set; }
        public Status status { get; set; }
        public string command { get; set; }
        //public Result result { get; set; }
    }
}
