﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models.MobileMoney
{
    public class TransactionListResponse : ResponseData
    {
        public List<TransactionListModel> transactionlist { get; set; }
    }
}
