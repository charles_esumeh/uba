﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Uba.Models;

namespace Uba.Core.Models.MobileMoney
{
    public class ResponseData
    {
        public string code { get; set; }
        public MessageModel message { get; set; }
        public string status { get; set; }
        public string error { get; set; }
        //[DataMember(Name = "status")]
        //public StatusError status { get; set; }
        //public string command { get; set; }
        //public string result { get; set; }
    }
}
