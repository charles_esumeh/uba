﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Models.MobileMoney
{
    public class TelcoResponse : ResponseData
    {
        public List<TelcoMessage> messages { get; set; }
    }
}
