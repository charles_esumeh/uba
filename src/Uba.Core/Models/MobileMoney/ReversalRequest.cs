﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class ReversalRequest : FundWallet
    {
        [Required]
        public string FormalRequestID { get; set; }
    }
}
