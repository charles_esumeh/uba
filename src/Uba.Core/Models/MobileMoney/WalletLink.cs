﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class WalletLink : WalletClass
    {
        public string TelcoSecurityCode { get; set; }
        [Required]
        public string DigitCurrencyCode { get; set; }
    }
}
