﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class FundWallet : WalletClass
    {
        [Required]
        public string Amount { get; set; }
        [Required]
        public string Narration { get; set; }

    }
}
