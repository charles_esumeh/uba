﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class MessageModel
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string msisdn { get; set; }
        public string account_status { get; set; }
        public string wallet { get; set; }
        public string smessage { get; set; }
        public string ticketid { get; set; }
        public string ttransid { get; set; }
        public string telcos { get; set; }
        public string currency { get; set; }
    }
}
