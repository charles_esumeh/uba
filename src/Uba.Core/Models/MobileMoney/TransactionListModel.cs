﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Models.MobileMoney
{
    public class TransactionListModel
    {
        public string transactionid { get; set; }
        public string date { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string walllettransid { get; set; }
        public string wallet { get; set; }
        public string walletuserid { get; set; }
        public string country { get; set; }
        public string status { get; set; }
    }
}
