﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Interfaces;
using Uba.Core.Models.MobileMoney;

namespace Uba.Core.Services
{
    public class WalletService : IWalletService
    {
        private static IWallet _getWallet;

        public WalletService(IWallet getWallet)
        {
            _getWallet = getWallet;
        }

        public async Task<ResponseData> ValidateAccount(WalletRequest wallet)
        {
            return await _getWallet.ValidateAccount(wallet);
        }

        public async Task<ResponseData> FundWallet(FundWallet wallet)
        {
            return await _getWallet.FundWallet(wallet);
        }

        public async Task<TransactionListResponse> ListTransaction(ListTransactions wallet)
        {
            return await _getWallet.ListTransaction(wallet);
        }

        public async Task<ResponseData> TransactionStatus(TxnStatusRequest txns)
        {
            return await _getWallet.TransactionStatus(txns);
        }

        public async Task<ResponseData> LinkWallet(WalletLink wallet)
        {
            return await _getWallet.LinkWallet(wallet);
        }

        public async Task<ResponseData> Balance(BalanceRequest wallet)
        {
            return await _getWallet.GetBalance(wallet);
        }

        public async Task<ResponseData> Reversal(ReversalRequest txn)
        {
            return await _getWallet.Reverse(txn);
        }

        public async Task<TelcoResponse> CheckTelco(BalanceRequest wallet)
        {
            return await _getWallet.CheckTelco(wallet);
        }
    }
}
