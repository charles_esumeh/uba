﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Interfaces;
using Uba.Core.Models;

namespace Uba.Core.Services
{
    public class AccountService : IAccountService
    {
        private static IAccount _getAccount;

        public AccountService(IAccount getAccount)
        {
            _getAccount = getAccount;
        }

        public async Task<AccountResponse> ReturnAccountInfo(AccountRequest account)
        {
            return await _getAccount.ReturnAccount(account);
        }

        public async Task<CreditResponse> CreditAccount(CreditRequest account)
        {
            return await _getAccount.CreditAccount(account);
        }

        public async Task<StatusResponse> GetStatus(StatusRequest account)
        {
            return await _getAccount.AccountStatus(account);
        }

    }
}
