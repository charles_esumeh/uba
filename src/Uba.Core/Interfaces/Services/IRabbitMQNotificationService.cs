﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Uba.Core.Interfaces.Services
{
    public interface IRabbitMQNotificationService
    {
        Task<bool> SendNotification(ErrorLogRequestViewModel t);
        //Task<bool> QueueProviderNotification(ErrorLogRequestViewModel model);
        //Task<bool> SendLog(ErrorLogRequestViewModel t);
        //Task<bool> SendNotificationError(ErrorLogRequestViewModel t);
    }
}
