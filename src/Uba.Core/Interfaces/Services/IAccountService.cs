﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Models;

namespace Uba.Core.Services
{
    public interface IAccountService
    {
        Task<AccountResponse> ReturnAccountInfo(AccountRequest account);
        Task<CreditResponse> CreditAccount(CreditRequest account);
        Task<StatusResponse> GetStatus(StatusRequest account);
    }
}
