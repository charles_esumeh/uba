﻿namespace Uba.Core.Interfaces.Services
{
    public class ErrorLogRequestViewModel
    {
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string PartnerCode { get; set; }
        public string GatewayCode { get; set; }
        public string TransactionReference { get; set; }
    }
}