﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Models.MobileMoney;

namespace Uba.Core.Services
{
    public interface IWalletService
    {
        Task<ResponseData> ValidateAccount(WalletRequest wallet);
        Task<ResponseData> LinkWallet(WalletLink wallet);
        Task<ResponseData> FundWallet(FundWallet wallet);
        Task<TransactionListResponse> ListTransaction(ListTransactions txns);
        Task<ResponseData> TransactionStatus(TxnStatusRequest txns);
        Task<ResponseData> Balance(BalanceRequest wallet);
        Task<ResponseData> Reversal(ReversalRequest reverse);
        Task<TelcoResponse> CheckTelco(BalanceRequest wallet);

    }
}
