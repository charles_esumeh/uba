﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Models;
using Uba.Core.Models.MobileMoney;

namespace Uba.Core.Interfaces
{
    public interface IWallet
    {
        Task<ResponseData> ValidateAccount(WalletRequest wallet);
        Task<ResponseData> FundWallet(FundWallet wallet);
        Task<TransactionListResponse> ListTransaction(ListTransactions wallet);
        Task<ResponseData> TransactionStatus(TxnStatusRequest txns);
        Task<ResponseData> LinkWallet(WalletLink wallet);
        Task<ResponseData> GetBalance(BalanceRequest wallet);
        Task<ResponseData> Reverse(ReversalRequest txn);
        Task<TelcoResponse> CheckTelco(BalanceRequest wallet);
    }
}
