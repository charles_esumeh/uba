﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Models;

namespace Uba.Core.Interfaces
{
    public interface IAccount
    {
        Task<AccountResponse> ReturnAccount(AccountRequest model);
        Task<CreditResponse> CreditAccount(CreditRequest model);
        Task<StatusResponse> AccountStatus(StatusRequest model);
    }
}
