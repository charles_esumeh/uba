﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uba.Core.Extension
{
    public class AppSettings
    {
        public string MobileMoneyURL { get; set; }
        public string RemittanceURL { get; set; }
        //public string Username { get; set; }
        //public string password { get; set; }
    }
}
