﻿using MassTransit;
using MassTransit.RabbitMqTransport.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using Uba.Api.Models;
using Uba.Core.Interfaces.Services;

namespace Uba.Core.Extension
{
    public static class ServiceExtensions
    { 

        public static void AddRabbitMQServices(this IServiceCollection services,
   IConfiguration Configuration, IServiceProvider serviceProvider)
        {
            // get rabbitmqconfig
            var rbmq = Configuration.GetSection("RabbitMQSettings").Get<RabbitMQSettings>();

            //services.AddMassTransit(c =>
            //{
            //    c.AddConsumer<PayOutConsumerService>();
            //});

            services.AddSingleton(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(rbmq.HostName), hostConfigurator =>
                {
                    hostConfigurator.Username(rbmq.UserName);
                    hostConfigurator.Password(rbmq.Password);
                });
            }));

            services.AddSingleton<IPublishEndpoint>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(provider => provider.GetRequiredService<IBusControl>());
            //services.AddScoped(provider => provider.GetRequiredService<IBus>().CreateRequestClient<VigiPayPayoutBrokerResponse>());
            //services.AddScoped(provider => provider.GetRequiredService<IBus>().CreateRequestClient<NotificationModel>());

            services.AddSingleton<IHostedService, BusService>();

        }

    }
}
