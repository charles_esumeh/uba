﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Uba.Core.Extension;
using Uba.Models;

namespace Uba
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        //private static string d;

        public void OnException(ExceptionContext context)
        {
            // _log.Information($"Error occured. Error details: {context.Exception.Message}, Stack Trace: {context.Exception.StackTrace}, Inner Exception Details: " + (context.Exception.InnerException == null ? context.Exception.Message : context.Exception.InnerException.Message));
            //Log.Error(context.Exception);
            var content = GetStatusCode<object>(context.Exception);
            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)content.Item2;
            response.ContentType = "application/json";

            context.Result = new JsonResult(content.Item1);
        }

        public static (ResponseModel responseModel, HttpStatusCode statusCode) GetStatusCode<T>(Exception exception)
        {
            switch (exception)
            {
                case BaseException bex:
                    return (new ResponseModel
                    {
                        ResponseCode = bex.Code,
                        Message = bex.Message,
                        RequestSuccessful = false,
                    }, bex.httpStatusCode);
                case SecurityTokenExpiredException bex:
                    return (new ResponseModel
                    {
                        ResponseCode = Constants.ResponseCodes.TokenExpired,
                        Message = "Session expired",
                        RequestSuccessful = false,
                    }, HttpStatusCode.Unauthorized);
                case ValidationException bex:
                    return (new ResponseModel
                    {
                        ResponseCode = Constants.ResponseCodes.ModelValidation,
                        Message = bex.Message,
                        RequestSuccessful = false,
                    }, HttpStatusCode.BadRequest);
                case SecurityTokenValidationException bex:
                    return (new ResponseModel
                    {
                        ResponseCode = Constants.ResponseCodes.TokenValidationFailed,
                        Message = "Invalid authentication parameters",
                        RequestSuccessful = false,
                    }, HttpStatusCode.Unauthorized);
                default:
                    return (new ResponseModel
                    {
                        ResponseCode = Constants.ResponseCodes.Failed,
                        Message = exception.Message,
                        RequestSuccessful = false
                    }, HttpStatusCode.InternalServerError);
            }
        }
    }
}