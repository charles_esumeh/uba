﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Uba.Core.Extension
{
    public static class Constants
    {
        public const string VigipayCode = "VGP";

        public static string GetDescription(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }

        public static class ResponseCodes
        {
            public const string Successful = "00";
            public const string Failed = "99";
            public const string AlreadyExist = "02";
            public const string NotFound = "03";
            public const string InvalidIssuer = "04";
            public const string TokenExpired = "05";
            public const string TokenValidationFailed = "06";
            public const string InvalidAudience = "07";
            public const string Unauthorized = "08";
            public const string ModelValidation = "09";
            public const string Error = "91";
            public const string Processing = "11";
        }

        public static string GetTnxReference(this long Indx)
        {
            return $"{VigipayCode}-{Indx}-{DateTime.Now.ToString("yyMMddhhmmss")}";
        }


        //public static Dictionary<string, string> ResponseCodes = new Dictionary<string, string>()
        //{
        //    { "RE05","Pending" },
        //    { "XXX","Need to confirm status from finacle" },
        //    { "000","Financial transaction has been approved" },
        //    { "111","Invalid scheme type" },
        //    { "114","Invalid account number" },
        //    { "115","Requested function not supported(First two digits of processing code or Function code is invalid)" },
        //    { "116","Insufficient Funds" },
        //    { "119","Transaction not permitted to card holder" },
        //    { "121","Withdrawal amount limit exceeded" },
        //    { "163","Invalid Cheque Status" },
        //    { "180","Transfer Limit Exceeded" },
        //    { "181","Cheques are in different books" },
        //    { "182","Not all cheques could be stopped" },
        //    { "183","Cheque not issued to this account" },
        //    { "184","Requested Block operation failed since Account is closed/frozen" },
        //    { "185","Invalid Currency/Transaction Amount" },
        //    { "186","Block does not exist" },
        //    { "187","Cheque Stopped" },
        //    { "188","Invalid Rate Currency Combination" },
        //    { "189","Cheque Book Already Issued" },
        //    { "190","DD Already Paid" },
        //    { "800","Network Message was accepted" },
        //    { "902","The request could not be understood by the server due to malformed syntax" },
        //    { "904","Format Error" },
        //    { "906","Cut-over in progress" },
        //    { "907","Card issuer inoperative" },
        //    { "909","System malfunction" },
        //    { "911","Card issuer timed out" },
        //    { "913","Duplicate transmission" },
        //    { "995","Invalid TransactionID" }
        //};


    }

}
