﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Extension
{
    public class RabbitMQSettings
    {
        public string HostName { get; set; }
        public string HostPort { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NotificationTopic { get; set; }
        public string UBATnxTopic { get; set; }
        public string UBANotificationTopic { get; set; }
        public string IndexingLogTopic { get; set; }
        public string RouterErrorLogTopic { get; set; }
    }
}
