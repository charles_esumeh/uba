﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Core.Extension
{
    public class CountryCodes { 
        public static string Country(string countryCode)
        {
            switch (countryCode)
            {
                case "NIGERIA":
                    return "NG";
                case "CHAD":
                    return "TD";
                case "CAMEROON":
                    return "CM";
                case "GUINEA":
                    return "GN";
                case "SIERRA LEONE":
                    return "SL";
                case "COTE D'IVOIRE":
                    return "CI";
                case "BURKINA FASO":
                    return "BF";
                case "ZAMBIA":
                    return "ZM";
                case "MZ":
                    return "MOZAMBIQUE";
                case "TANZANIA":
                    return "TZ";
                case "GHANA":
                    return "GH";
                case "SENEGAL":
                    return "SN";
                case "CONGO BRAZAVILLE":
                    return "CG";
                case "KENYA":
                    return "KE";
                case "GABON":
                    return "GA";
                case "BENIN":
                    return "BJ";
                case "LIBERIA":
                    return "LR";
                case "MALI":
                    return "ML";
                case "CONGO-KINSHASA":
                    return "CD";
                case "UGANDA":
                    return "UG";
                case "MAURITIUS":
                    return "MU";
                default: return countryCode;
            }
        }

        private Dictionary<string, string> GetCountryCode()
        {
            return new Dictionary<string, string>
            {
                {"NG", "NIGERIA"},
                {"TD", "CHAD"},
                {"CM", "CAMEROON"},
                {"GN", "GUINEA"},
                {"SL", "SIERRA LEONE"},
                {"CI", "COTE D'IVOIRE"},
                {"BF", "BURKINA FASO"},
                {"ZM", "ZAMBIA"},
                {"MZ", "MOZAMBIQUE"},
                {"TZ", "TANZANIA"},
                {"GH", "GHANA"},
                {"SN", "SENEGAL"},
                {"CG", "CONGO BRAZAVILLE"},
                {"KE", "KENYA"},
                {"GA", "GABON"},
                {"BJ", "BENIN"},
                {"LR", "LIBERIA"},
                {"ML", "MALI"},
                {"CD", "CONGO-KINSHASA"},
                {"UG", "UGANDA"},
                {"MU", "MAURITIUS"}
            };
        }
    }

}
