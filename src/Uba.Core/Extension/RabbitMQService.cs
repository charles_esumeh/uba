﻿using MassTransit;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Interfaces.Services;

namespace Uba.Core.Extension
{
    public class RabbitMQService : IRabbitMQNotificationService
    {
        private readonly RabbitMQSettings _rqSettings;
        private readonly IBus _bus;
        public RabbitMQService(IBus bus,
            IOptions<RabbitMQSettings> rqSettings)
        {
            _rqSettings = rqSettings.Value;
            _bus = bus;
        }

        public async Task<bool> SendNotification(ErrorLogRequestViewModel t)
        {
            Uri url = new Uri(new Uri(_rqSettings.HostName), _rqSettings.NotificationTopic);
            var sendEndpoint = await _bus.GetSendEndpoint(url);
            await sendEndpoint.Send(t);
            return true;
        }

        //public async Task<bool> SendNotificationError(ErrorLogRequestViewModel t)
        //{
        //    Uri url = new Uri(new Uri(_rqSettings.HostName), _rqSettings.RouterErrorLogTopic);
        //    var sendEndpoint = await _bus.GetSendEndpoint(url);
        //    await sendEndpoint.Send(t);
        //    return true;
        //}

        //public async Task<bool> QueueProviderNotification(ErrorLogRequestViewModel model)
        //{
        //    var sendEndpoint = await _bus.GetSendEndpoint(new Uri($"{_rqSettings.HostName}{_rqSettings.UBANotificationTopic}"));
        //    await sendEndpoint.Send(model);
        //    return true;
        //}

        //public async Task<bool> SendLog(ErrorLogRequestViewModel t)
        //{
        //    Uri url = new Uri(new Uri(_rqSettings.HostName), _rqSettings.IndexingLogTopic);
        //    var sendEndpoint = await _bus.GetSendEndpoint(url);
        //    await sendEndpoint.Send(t);
        //    return true;
        //}

    }

}
