﻿using System.Collections.Generic;

namespace Uba.Core.Extension
{
    public class ErrorCodes
    {

        public static Dictionary<string, string> responseCode = new Dictionary<string, string>()
        {
            { "RE05","Pending" },
            { "XXX","Need to confirm status from finacle" },
            { "000","Financial transaction has been approved" },
            { "111","Invalid scheme type" },
            { "114","Invalid account number" },
            { "115","Requested function not supported(Firts two digits of processing code or Function code is invalid)" },
            { "116","Insufficient Funds" },
            { "119","Transaction not permitted to card holder" },
            { "121","Withdrawal amount limit exceeded" },
            { "163","Invalid Cheque Status" },
            { "180","Transfer Limit Exceeded" },
            { "181","Cheques are in different books" },
            { "182","Not all cheques could be stopped" },
            { "183","Cheque not issued to this account" },
            { "184","Requested Block operation failed since Account is closed/frozen" },
            { "185","Invalid Currency/Transaction Amount" },
            { "186","Block does not exist" },
            { "187","Cheque Stopped" },
            { "188","Invalid Rate Currency Combination" },
            { "189","Cheque Book Already Issued" },
            { "190","DD Already Paid" },
            { "800","Network Message was accepted" },
            { "902","The request could not be understood by the server due to malformed syntax" },
            { "904","Format Error" },
            { "906","Cut-over in progress" },
            { "907","Card issuer inoperative" },
            { "909","System malfunction" },
            { "911","Card issuer timed out" },
            { "913","Duplicate transmission" },
            { "990","RecoverableException. Java Exception calling CallStaticObjectMethodAJava"},
            { "995","Invalid TransactionID" }
        };

        //public static string Codes (string statuscode)
        //{
        //    switch (statuscode)
        //    {
        //        case "RE05":
        //            return "Pending";
        //        case "000":
        //            return "Financial transaction has been approved";
        //        case "111":
        //            return "Invalid scheme type";
        //        case "114":
        //            return "Invalid account number";
        //        case "115":
        //            return "Requested function not supported(Firts two digits of processing code or Function code is invalid)";
        //        case "116":
        //            return "Insufficient Funds";
        //        case "119":
        //            return "Transaction not permitted to card holder";
        //        case "121":
        //            return "Withdrawal amount limit exceeded";
        //        case "163":
        //            return "Invalid Cheque Status";
        //        case "180":
        //            return "Transfer Limit Exceeded";
        //        case "181":
        //            return "Cheques are in different books";
        //        case "182":
        //            return "Not all cheques could be stopped";
        //        case "183":
        //            return "Cheque not issued to this account";
        //        case "184":
        //            return "Requested Block operation failed since Account is closed/frozen";
        //        case "185":
        //            return "Invalid Currency/Transaction Amount";
        //        case "186":
        //            return "Block does not exist";
        //        case "187":
        //            return "Cheque Stopped";
        //        case "188":
        //            return "Invalid Rate Currency Combination";
        //        case "189":
        //            return "Cheque Book Already Issued";
        //        case "190":
        //            return "DD Already Paid";
        //        case "800":
        //            return "Network Message was accepted";
        //        case "902":
        //            return "Invalid transaction";
        //        case "904":
        //            return "Format Error";
        //        case "906":
        //            return "Cut-over in progress";
        //        case "907":
        //            return "Card issuer inoperative";
        //        case "909":
        //            return "System malfunction";
        //        case "911":
        //            return "Card issuer timed out";
        //        case "913":
        //            return "Duplicate transmission";
        //        case "995":
        //            return "Invalid TransactionID";
        //        case "XXX":
        //            return "Need to confirm status from finacle";
        //        default:
        //            return statuscode;
        //    }
        //}
    }
}
