﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Extension;
using Uba.Core.Models.MobileMoney;

namespace Uba.Core.Interfaces
{
    public class Wallet : IWallet
    {
        private readonly IOptions<AppSettings> _appSettings;

        private readonly ILoggerService _logger;

        public Wallet(IOptions<AppSettings> appSettings, ILoggerService logger)
        {
            _appSettings = appSettings;
            _logger = logger;
        }
        public async Task<ResponseData> ValidateAccount(WalletRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"validatemma", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }

        public async Task<ResponseData> FundWallet(FundWallet account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"banktomma", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public async Task<ResponseData> LinkWallet(WalletLink account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"mmabanklink", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public async Task<TelcoResponse> CheckTelco(BalanceRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UTF8Encoding.UTF8, "application/json");
                    var response = await client.PostAsync($"checktelco", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TelcoResponse>(stringResult);

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public async Task<ResponseData> Reverse(ReversalRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"Reversal", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }

        public async Task<ResponseData> GetBalance(BalanceRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"balance", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public async Task<ResponseData> TransactionStatus(TxnStatusRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"transactionStatus", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseData>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public async Task<TransactionListResponse> ListTransaction(ListTransactions account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.MobileMoneyURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"listTransaction", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TransactionListResponse>(stringResult);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }
    }
}
