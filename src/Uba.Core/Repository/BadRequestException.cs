﻿namespace Uba.Core.Interfaces
{
    internal class BadRequestException
    {
        private string json;

        public BadRequestException(string json)
        {
            this.json = json;
        }
    }
}