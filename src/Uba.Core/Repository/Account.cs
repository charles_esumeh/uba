﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NLog.Fluent;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Uba.Core.Extension;
using Uba.Core.Models;

namespace Uba.Core.Interfaces
{
    public class Account : IAccount
    {
        //private readonly AppSettings Url = new AppSettings();
        private readonly IOptions<AppSettings> _appSettings;

        private readonly ILoggerService _logger;

        public Account(IOptions<AppSettings> appSettings, ILoggerService logger)
        {
            _appSettings = appSettings;
            _logger = logger;
        }
        public async Task<AccountResponse> ReturnAccount(AccountRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.RemittanceURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"accountinformation", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<AccountResponse>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //Log.Error(ex.ToString());
                throw ex;
            }

        }

        public async Task<CreditResponse> CreditAccount(CreditRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.RemittanceURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"credit", content);
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }
                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CreditResponse>(stringResult);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //Log.Error(ex.ToString());
                throw ex;
            }

        }

        public async Task<StatusResponse> AccountStatus(StatusRequest account)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_appSettings.Value.RemittanceURL);
                    var json = JsonConvert.SerializeObject(account);
                    HttpContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    var response = await client.PostAsync($"Status", content);

                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.Write(response.ToString());
                    }

                    var stringResult = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<StatusResponse>(stringResult);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //Log.Error(ex.ToString());
                throw ex;


            }
        }
    }
}
