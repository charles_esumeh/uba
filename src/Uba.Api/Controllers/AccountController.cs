﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uba.Data;
using Uba.Core;
using Uba.Core.Extension;
using Uba.Core.Models;
using Uba.Core.Services;
using Uba.Models;
using Uba.Data.Entity;

namespace Uba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AppDbContext _context;

        private string d = "";

        private readonly IAccountService _accountServices;

        public AccountController(IAccountService accountServices, AppDbContext context)
        {
            _accountServices = accountServices;
            _context = context;
        }

        [HttpPost("getaccount")]
        [ProducesResponseType(typeof(StatusCodes), 200)]
        public async Task<IActionResult> GetAccount([FromBody]AccountRequest model)
        {

            var result = await _accountServices.ReturnAccountInfo(model);

            if (result.ErrorCode == null)
            {
                d = result.accountInformation.responseCode;
            }
            else
            {
                d = result.ErrorCode;
            }

            //return Ok(result);
            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("creditaccount")]
        [ProducesResponseType(typeof(StatusCodes), 201)]
        public async Task<IActionResult> CreditAccount([FromBody]CreditRequest model)
        {
            var result = await _accountServices.CreditAccount(model);

            TransactionRequest transactionRequest = model.Map();

            Remittance txn = transactionRequest.MapR();

            txn.maxCompletionDate = DateTime.Now;
            txn.UBATransactionId = result.transactionId;
            txn.VigipayReference = txn.Id.GetTnxReference();
            if (result.ErrorCode != null)
            {
                txn.responseMessage = txn.ErrorDescription = result.ErrorDescription;
                txn.responseCode = txn.ErrorCode = result.ErrorCode;
                d = result.ErrorCode;
                if (result.ErrorCode == "000")
                {
                    txn.CreatedBy = txn.ModifiedBy = "VIGIPAY";
                    txn.UpdatedAt = DateTime.Now;
                    RecordStatus status = RecordStatus.Successful;
                    txn.RecordStatus = status;
                }
                else
                {
                    txn.CreatedBy = txn.ModifiedBy = "VIGIPAY";
                    txn.UpdatedAt = DateTime.Now;
                }
            }
            else
            {
                txn.responseMessage = result.provisionalResponse.label;
                txn.responseCode = result.provisionalResponse.state;
            }
            _context.Accounts.Add(txn);
            await _context.SaveChangesAsync();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("getstatus")]
        [ProducesResponseType(typeof(StatusCodes), 200)]
        public async Task<IActionResult> GetStatus(StatusRequest model)
        {
            var result = await _accountServices.GetStatus(model);

            if (result.ErrorCode != null)
            {
                d = result.ErrorCode;
            }
            else
            {
                d = "000";
            }

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }
    }
}