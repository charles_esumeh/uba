﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uba.Data;
using Uba.Core.Extension;
using Uba.Data.Entity;
using Uba.Core.Models.MobileMoney;
using Uba.Core.Services;
using Uba.Models;
using Uba.Core.Models;
using Microsoft.Extensions.Logging;

namespace Uba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : ControllerBase
    {

        private readonly IWalletService _walletServices;

        private readonly AppDbContext _context;

        //private readonly ILogger<WalletController> _logger;

        public WalletController(IWalletService walletServices, AppDbContext context)
        {
            _walletServices = walletServices;
            _context = context;
        }

        [HttpPost("validateaccount")]
        [ProducesResponseType(typeof(StatusCodes), 200)]
        public async Task<IActionResult> ValidateAccount([FromBody]WalletRequest wallet)
        {
            var result = await _walletServices.ValidateAccount(wallet);
            
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("linkwallet")]
        [ProducesResponseType(typeof(StatusCodes), 201)]
        public async Task<IActionResult> LinkWallet([FromBody]WalletLink wallet)
        {
            var result = await _walletServices.LinkWallet(wallet);
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("fundwallet")]
        [ProducesResponseType(typeof(StatusCodes), 201)]
        public async Task<IActionResult> FundWallet([FromBody]FundWallet wallet)
        {
            var result = await _walletServices.FundWallet(wallet);

            MobileMoney mobileMoney = wallet.MapF();

            mobileMoney.Code = result.code;
            mobileMoney.Status = result.status;
            mobileMoney.SMessage = result.message.smessage;
            mobileMoney.TTransId = result.message.ttransid;
            mobileMoney.VigipayReference = mobileMoney.Id.GetTnxReference();

            _context.Wallets.Add(mobileMoney);
            await _context.SaveChangesAsync();

            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("listtransaction")]
        [ProducesResponseType(typeof(StatusCodes), 200)]
        public async Task<IActionResult> ListTransaction([FromBody]ListTransactions wallet)
        {
            var result = await _walletServices.ListTransaction(wallet);
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("transactionstatus")]
        [ProducesResponseType(typeof(StatusCodes), 200)]
        public async Task<IActionResult> TransactionStatus([FromBody]TxnStatusRequest wallet)
        {
            var result = await _walletServices.TransactionStatus(wallet);
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("balance")]
        public async Task<IActionResult> GetBalance([FromBody]BalanceRequest wallet)
        {
            var result = await _walletServices.Balance(wallet);
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("reversal")]
        public async Task<IActionResult> Reversal([FromBody]ReversalRequest wallet)
        {
            var result = await _walletServices.Reversal(wallet);

            MobileMoney mobileMoney = Mapper.MapF(wallet);

            mobileMoney.Code = result.code;
            mobileMoney.Status = result.status;
            mobileMoney.SMessage = result.message.smessage;
            mobileMoney.TTransId = result.message.ttransid;
            mobileMoney.VigipayReference = mobileMoney.Id.GetTnxReference();
            mobileMoney.FormalRequestID = wallet.RequestID;

            _context.Wallets.Add(mobileMoney);
            await _context.SaveChangesAsync();

            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }

        [HttpPost("checktelco")]
        public async Task<IActionResult> CheckTelco([FromBody]BalanceRequest wallet)
        {
            var result = await _walletServices.CheckTelco(wallet);
            string d = result.error.ToString();

            return Ok(new ResponseModel
            {
                RequestSuccessful = true,
                Message = ErrorCodes.responseCode[d],
                ResponseCode = d,
                ResponseData = result
            });
        }
    }
}