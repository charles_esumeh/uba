﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Uba.Core;
using Uba.Data.Entity;
using Uba.Core.Models.MobileMoney;
using Uba.Data;

namespace Uba.Models
{
    public static class Mapper
    {
        public static TransactionRequest Map(this CreditRequest model)
        {
            return new TransactionRequest
            {
                login = model.Security.login,
                Password = model.Security.Password,
                transactionId = model.transactionId,
                hstransactionId = model.hstransactionId,
                sourceUri = model.sourceUri,
                destinationUri = model.destinationUri,
                Stan = model.Stan,
                currency = model.amountInformation.currency,
                amount = model.amountInformation.amount,
                grace = model.amountInformation.grace,
                validity = model.amountInformation.validity,
                Name = model.senderInformation.Name,
                Address = model.senderInformation.Address,
                SourceIban = model.senderInformation.SourceIban,
                SourceCode = model.senderInformation.SourceCode,
                routingTag = model.routingTag,
                description = model.description,
                ChannelCode = model.ChannelCode,
                ClientId = model.vendorSpecificFields.ClientId,
                fieldId = model.vendorSpecificFields.fieldId,
                ClientName = model.vendorSpecificFields.ClientName,
                value = model.vendorSpecificFields.value,
                BenefBank = model.vendorSpecificFields.BenefBank,
                BenefBranch = model.vendorSpecificFields.BenefBranch,
                BeneficiaryIban = model.vendorSpecificFields.BeneficiaryIban,
                BenefCode = model.vendorSpecificFields.BenefCode,
                BenefName = model.vendorSpecificFields.BenefName,
                BenefSortCode = model.vendorSpecificFields.BenefSortCode,
                reportStatusTo = model.provisionalResponseSupport.reportStatusTo,
                token = model.provisionalResponseSupport.token
            };
        }

        public static Remittance MapR(this TransactionRequest model)
        {
            return new Remittance
            {
                login = model.login,
                Password = model.Password,
                transactionId = model.transactionId,
                hstransactionId = model.hstransactionId,
                sourceUri = model.sourceUri,
                destinationUri = model.destinationUri,
                Stan = model.Stan,
                currency = model.currency,
                amount = model.amount,
                grace = model.grace,
                validity = model.validity,
                Name = model.Name,
                Address = model.Address,
                SourceIban = model.SourceIban,
                SourceCode = model.SourceCode,
                routingTag = model.routingTag,
                description = model.description,
                ChannelCode = model.ChannelCode,
                ClientId = model.ClientId,
                fieldId = model.fieldId,
                ClientName = model.ClientName,
                value = model.value,
                BenefBank = model.BenefBank,
                BenefBranch = model.BenefBranch,
                BeneficiaryIban = model.BeneficiaryIban,
                BenefCode = model.BenefCode,
                BenefName = model.BenefName,
                BenefSortCode = model.BenefSortCode,
                reportStatusTo = model.reportStatusTo,
                token = model.token

            };
        }

        public static MobileMoney MapF(this FundWallet model)
        {
            return new MobileMoney
            {
                Amount = model.Amount,
                Narration = model.Narration, 
                AccountNumber = model.AccountNumber,
                CountryCode = model.CountryCode,
                Provider = model.Provider,
                DebitAccount = model.DebitAccount,
                FirstName = model.FirstName,
                LastName = model.LastName,
                CurrencyCode = model.CurrencyCode,
                AccountType = model.AccountType,
                RequestID = model.RequestID

            };
        }

    }

}
