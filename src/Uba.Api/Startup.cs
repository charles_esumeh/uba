﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackifyLib;
using StackifyLib.CoreLogger;
using Uba.Data;
using Uba.Core.Extension;
using Uba.Core.Interfaces;
using Uba.Core.Services;
using Uba.Models;

namespace Uba
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
                Configuration = builder.Build();


            StackifyLib.Config.Environment = env.EnvironmentName; //optional
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            IServiceProvider serviceProvider = services.BuildServiceProvider();
            services.AddCors();
            services.AddHttpClient();
            services.AddMvc(config =>
            {
                config.Filters.Add(new GlobalExceptionFilter());
                config.OutputFormatters.RemoveType<TextOutputFormatter>();
                config.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
            }).AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Formatting = Formatting.Indented;
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton<IAccountService, AccountService>();
            services.AddSingleton<IAccount, Account>();
            services.AddSingleton<IWalletService, WalletService>();
            services.AddSingleton<IWallet, Wallet>();
            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));

            services.AddRabbitMQServices(Configuration, serviceProvider);
            services.AddSingleton<ILoggerService, LoggerService>();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Uba Integration", Version = "v2" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddStackify(); //add the provider
            app.ConfigureStackifyLogging(Configuration); //This is critical!!

            app.UseMiddleware<StackifyMiddleware.RequestTracerMiddleware>();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "UBA API v2");
                c.RoutePrefix = "";
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //add NLog to ASP.NET Core
            //loggerFactory.AddNLog();

            app.UseHttpsRedirection();
            app.UseCors(x => x
                 .AllowAnyOrigin()
                 .AllowAnyMethod()
                 .AllowAnyHeader());
            app.UseExceptionHandler(builder =>
            {
                builder.Run(
                    async context =>
                    {
                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        var exception = error.Error;

                       // Log.Error(exception);

                        var (responseModel, statusCode) = GlobalExceptionFilter.GetStatusCode<object>(exception);
                        context.Response.StatusCode = (int)statusCode;
                        context.Response.ContentType = "application/json";

                        var responseJson = JsonConvert.SerializeObject(responseModel, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                        await context.Response.WriteAsync(responseJson);
                    });
            });

            //app.UseExceptionHandler(errorApp =>
            //{
            //    errorApp.Run(async context =>
            //    {
            //        var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
            //        var exception = errorFeature.Error;

            //        // the IsTrusted() extension method doesn't exist and
            //        // you should implement your own as you may want to interpret it differently
            //        // i.e. based on the current principal

            //        var problemDetails = new ProblemDetails
            //        {
            //            Instance = $"urn:VigiPay:error:{Guid.NewGuid()}"
            //        };

            //        if (exception is BadHttpRequestException badHttpRequestException)
            //        {
            //            problemDetails.Title = "Invalid request";
            //            problemDetails.Status = (int)typeof(BadHttpRequestException).GetProperty("StatusCode",
            //                BindingFlags.NonPublic | BindingFlags.Instance).GetValue(badHttpRequestException);
            //            problemDetails.Detail = badHttpRequestException.Message;
            //        }
            //        else
            //        {
            //            problemDetails.Title = "An unexpected error occurred!";
            //            problemDetails.Status = 500;
            //            problemDetails.Detail = exception.Message.ToString();
            //        }

            //        // log the exception etc..

            //        context.Response.StatusCode = problemDetails.Status.Value;
            //        context.Response.WriteJson(problemDetails, "application/problem+json");
            //    });
            //});
            //app.UseApiResponseAndExceptionWrapper<ResponseModel>(new AutoWrapperOptions { ShowStatusCode = true, IsDebug = true });
            app.UseMvc();

        }
    }
}
