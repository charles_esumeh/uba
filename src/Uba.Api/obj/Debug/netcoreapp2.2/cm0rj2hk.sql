﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Remittance] (
    [Id] int NOT NULL IDENTITY,
    [ClientId] nvarchar(max) NULL,
    [login] nvarchar(max) NULL,
    [Password] nvarchar(max) NULL,
    [transactionId] nvarchar(max) NULL,
    [UBATransactionId] nvarchar(max) NULL,
    [hstransactionId] nvarchar(max) NULL,
    [sourceUri] nvarchar(max) NULL,
    [destinationUri] nvarchar(max) NULL,
    [Stan] nvarchar(max) NULL,
    [currency] nvarchar(max) NULL,
    [amount] nvarchar(max) NULL,
    [grace] nvarchar(max) NULL,
    [validity] nvarchar(max) NULL,
    [Name] nvarchar(max) NULL,
    [Address] nvarchar(max) NULL,
    [SourceIban] nvarchar(max) NULL,
    [SourceCode] nvarchar(max) NULL,
    [routingTag] nvarchar(max) NULL,
    [description] nvarchar(max) NULL,
    [ChannelCode] nvarchar(max) NULL,
    [fieldId] nvarchar(max) NULL,
    [value] nvarchar(max) NULL,
    [BeneficiaryIban] nvarchar(max) NULL,
    [BenefName] nvarchar(max) NULL,
    [BenefBank] nvarchar(max) NULL,
    [BenefBranch] nvarchar(max) NULL,
    [BenefSortCode] nvarchar(max) NULL,
    [BenefCode] nvarchar(max) NULL,
    [reportStatusTo] nvarchar(max) NULL,
    [token] nvarchar(max) NULL,
    [ClientName] nvarchar(max) NULL,
    [maxCompletionDate] datetime2 NOT NULL,
    [state] nvarchar(max) NULL,
    [label] nvarchar(max) NULL,
    [responseCode] nvarchar(max) NULL,
    [responseMessage] nvarchar(max) NULL,
    [balanceCurrency] nvarchar(max) NULL,
    [accountName] nvarchar(max) NULL,
    CONSTRAINT [PK_Remittance] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200225124952_InitialMigration', N'2.2.6-servicing-10079');

GO

ALTER TABLE [Remittance] ADD [ErrorCode] nvarchar(max) NULL;

GO

ALTER TABLE [Remittance] ADD [ErrorDescription] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200225143815_UpdateRemittance', N'2.2.6-servicing-10079');

GO

CREATE TABLE [MobileMoney] (
    [Id] int NOT NULL IDENTITY,
    [AccountNumber] nvarchar(max) NULL,
    [Provider] nvarchar(max) NULL,
    [CountryCode] nvarchar(max) NULL,
    [TeleSecurityCode] nvarchar(max) NULL,
    [Code] nvarchar(max) NULL,
    [Status] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [MSISDN] nvarchar(max) NULL,
    [Account_Status] nvarchar(max) NULL,
    [Wallet] nvarchar(max) NULL,
    [DebitAccount] nvarchar(max) NULL,
    [CurrencyCode] nvarchar(max) NULL,
    [DigitCurrency] nvarchar(max) NULL,
    [RequestID] nvarchar(max) NULL,
    [Amount] nvarchar(max) NULL,
    [Narration] nvarchar(max) NULL,
    [TTransId] nvarchar(max) NULL,
    [SMessage] nvarchar(max) NULL,
    [Available] nvarchar(max) NULL,
    [Ledger] nvarchar(max) NULL,
    [FormalRequestID] nvarchar(max) NULL,
    CONSTRAINT [PK_MobileMoney] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200225150042_AddMobileMoney', N'2.2.6-servicing-10079');

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'RequestID');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [RequestID] nvarchar(max) NOT NULL;

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'Provider');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [Provider] nvarchar(max) NOT NULL;

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'Narration');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [Narration] nvarchar(max) NOT NULL;

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'LastName');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [LastName] nvarchar(max) NOT NULL;

GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'FormalRequestID');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [FormalRequestID] nvarchar(max) NOT NULL;

GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'FirstName');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [FirstName] nvarchar(max) NOT NULL;

GO

DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'DigitCurrency');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [DigitCurrency] nvarchar(max) NOT NULL;

GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'DebitAccount');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [DebitAccount] nvarchar(max) NOT NULL;

GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'CurrencyCode');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [CurrencyCode] nvarchar(max) NOT NULL;

GO

DECLARE @var9 sysname;
SELECT @var9 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'CountryCode');
IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var9 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [CountryCode] nvarchar(max) NOT NULL;

GO

DECLARE @var10 sysname;
SELECT @var10 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'Amount');
IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var10 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [Amount] nvarchar(max) NOT NULL;

GO

DECLARE @var11 sysname;
SELECT @var11 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MobileMoney]') AND [c].[name] = N'AccountNumber');
IF @var11 IS NOT NULL EXEC(N'ALTER TABLE [MobileMoney] DROP CONSTRAINT [' + @var11 + '];');
ALTER TABLE [MobileMoney] ALTER COLUMN [AccountNumber] nvarchar(max) NOT NULL;

GO

ALTER TABLE [MobileMoney] ADD [AccountType] nvarchar(max) NOT NULL DEFAULT N'';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200225154804_AddedMobileMoneyRequiredFields', N'2.2.6-servicing-10079');

GO

DECLARE @var12 sysname;
SELECT @var12 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Remittance]') AND [c].[name] = N'maxCompletionDate');
IF @var12 IS NOT NULL EXEC(N'ALTER TABLE [Remittance] DROP CONSTRAINT [' + @var12 + '];');
ALTER TABLE [Remittance] ALTER COLUMN [maxCompletionDate] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200225161701_UpdateMaxCompletionDate', N'2.2.6-servicing-10079');

GO

