﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Data.Entity
{
    public class Remittance : EntityBase<long>
    {
        //public int Id { get; set; }
        public string VigipayReference { get; set; }
        public string ClientId { get; set; }
        public string login { get; set; }
        public string Password { get; set; }
        public string transactionId { get; set; }
        public string UBATransactionId { get; set; }
        public string hstransactionId { get; set; }
        public string sourceUri { get; set; }
        public string destinationUri { get; set; }
        public string Stan { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string grace { get; set; }
        public string validity { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SourceIban { get; set; }
        public string SourceCode { get; set; }
        public string routingTag { get; set; }
        public string description { get; set; }
        public string ChannelCode { get; set; }
        public string fieldId { get; set; }
        public string value { get; set; }
        public string BeneficiaryIban { get; set; }
        public string BenefName { get; set; }
        public string BenefBank { get; set; }
        public string BenefBranch { get; set; }
        public string BenefSortCode { get; set; }
        public string BenefCode { get; set; }
        public string reportStatusTo { get; set; }
        public string token { get; set; }
        public string ClientName { get; set; }
        public DateTime? maxCompletionDate { get; set; } = DateTime.Now;
        public string state { get; set; }
        public string label { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public string balanceCurrency { get; set; }
        public string accountName { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}
