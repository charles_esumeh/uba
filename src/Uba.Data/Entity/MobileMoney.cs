﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Uba.Data.Entity
{
    public class MobileMoney : EntityBase<long>
    {
        //public long Id { get; set; }
        public string VigipayReference { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string Provider { get; set; }
        [Required]
        public string CountryCode { get; set; }
        public string TeleSecurityCode { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MSISDN { get; set; }
        public string Account_Status { get; set; }
        public string Wallet { get; set; }
        [Required]
        public string DebitAccount { get; set; }
        [Required]
        public string CurrencyCode { get; set; }
        [Required]
        public string DigitCurrency { get; set; }
        [Required]
        public string RequestID { get; set; }
        [Required]
        public string Amount { get; set; }
        [Required]
        public string Narration { get; set; }
        public string TTransId { get; set; }
        public string SMessage { get; set; }
        public string Available { get; set; }
        public string Ledger { get; set; }
        [Required]
        public string FormalRequestID { get; set; }
        [Required]
        public string AccountType { get; set; }

    }
}
