﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uba.Data.Entity
{
    public abstract class ReportingBase
    {
        public ReportingBase()
        {
            CreatedAt = DateTimeOffset.Now;
        }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}
