﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Uba.Data.Entity
{
    public class EntityBase<T> : ReportingBase
    {
        [Key]
        public T Id { get; set; }
        public RecordStatus RecordStatus { get; set; }
    }
}
