﻿namespace Uba.Data.Entity
{
    public enum RecordStatus
    {
        /// <summary>
        /// The pending
        /// </summary>
        Failed = 0,
        /// <summary>
        /// The active
        /// </summary>
        Successful
    }

}