﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [MobileMoney] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedAt] datetimeoffset NOT NULL,
    [UpdatedAt] datetimeoffset NULL,
    [CreatedBy] nvarchar(max) NULL,
    [ModifiedBy] nvarchar(max) NULL,
    [RecordStatus] int NOT NULL,
    [VigipayReference] nvarchar(max) NULL,
    [AccountNumber] nvarchar(max) NOT NULL,
    [Provider] nvarchar(max) NOT NULL,
    [CountryCode] nvarchar(max) NOT NULL,
    [TeleSecurityCode] nvarchar(max) NULL,
    [Code] nvarchar(max) NULL,
    [Status] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NOT NULL,
    [LastName] nvarchar(max) NOT NULL,
    [MSISDN] nvarchar(max) NULL,
    [Account_Status] nvarchar(max) NULL,
    [Wallet] nvarchar(max) NULL,
    [DebitAccount] nvarchar(max) NOT NULL,
    [CurrencyCode] nvarchar(max) NOT NULL,
    [DigitCurrency] nvarchar(max) NOT NULL,
    [RequestID] nvarchar(max) NOT NULL,
    [Amount] nvarchar(max) NOT NULL,
    [Narration] nvarchar(max) NOT NULL,
    [TTransId] nvarchar(max) NULL,
    [SMessage] nvarchar(max) NULL,
    [Available] nvarchar(max) NULL,
    [Ledger] nvarchar(max) NULL,
    [FormalRequestID] nvarchar(max) NOT NULL,
    [AccountType] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_MobileMoney] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Remittance] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedAt] datetimeoffset NOT NULL,
    [UpdatedAt] datetimeoffset NULL,
    [CreatedBy] nvarchar(max) NULL,
    [ModifiedBy] nvarchar(max) NULL,
    [RecordStatus] int NOT NULL,
    [VigipayReference] nvarchar(max) NULL,
    [ClientId] nvarchar(max) NULL,
    [login] nvarchar(max) NULL,
    [Password] nvarchar(max) NULL,
    [transactionId] nvarchar(max) NULL,
    [UBATransactionId] nvarchar(max) NULL,
    [hstransactionId] nvarchar(max) NULL,
    [sourceUri] nvarchar(max) NULL,
    [destinationUri] nvarchar(max) NULL,
    [Stan] nvarchar(max) NULL,
    [currency] nvarchar(max) NULL,
    [amount] nvarchar(max) NULL,
    [grace] nvarchar(max) NULL,
    [validity] nvarchar(max) NULL,
    [Name] nvarchar(max) NULL,
    [Address] nvarchar(max) NULL,
    [SourceIban] nvarchar(max) NULL,
    [SourceCode] nvarchar(max) NULL,
    [routingTag] nvarchar(max) NULL,
    [description] nvarchar(max) NULL,
    [ChannelCode] nvarchar(max) NULL,
    [fieldId] nvarchar(max) NULL,
    [value] nvarchar(max) NULL,
    [BeneficiaryIban] nvarchar(max) NULL,
    [BenefName] nvarchar(max) NULL,
    [BenefBank] nvarchar(max) NULL,
    [BenefBranch] nvarchar(max) NULL,
    [BenefSortCode] nvarchar(max) NULL,
    [BenefCode] nvarchar(max) NULL,
    [reportStatusTo] nvarchar(max) NULL,
    [token] nvarchar(max) NULL,
    [ClientName] nvarchar(max) NULL,
    [maxCompletionDate] datetime2 NULL,
    [state] nvarchar(max) NULL,
    [label] nvarchar(max) NULL,
    [responseCode] nvarchar(max) NULL,
    [responseMessage] nvarchar(max) NULL,
    [balanceCurrency] nvarchar(max) NULL,
    [accountName] nvarchar(max) NULL,
    [ErrorCode] nvarchar(max) NULL,
    [ErrorDescription] nvarchar(max) NULL,
    CONSTRAINT [PK_Remittance] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200227120826_InitialMigration', N'2.2.6-servicing-10079');

GO

