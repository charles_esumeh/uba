﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Uba.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20200225154804_AddedMobileMoneyRequiredFields")]
    partial class AddedMobileMoneyRequiredFields
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Uba.Data.Entity.MobileMoney", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AccountNumber")
                        .IsRequired();

                    b.Property<string>("AccountType")
                        .IsRequired();

                    b.Property<string>("Account_Status");

                    b.Property<string>("Amount")
                        .IsRequired();

                    b.Property<string>("Available");

                    b.Property<string>("Code");

                    b.Property<string>("CountryCode")
                        .IsRequired();

                    b.Property<string>("CurrencyCode")
                        .IsRequired();

                    b.Property<string>("DebitAccount")
                        .IsRequired();

                    b.Property<string>("DigitCurrency")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("FormalRequestID")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("Ledger");

                    b.Property<string>("MSISDN");

                    b.Property<string>("Narration")
                        .IsRequired();

                    b.Property<string>("Provider")
                        .IsRequired();

                    b.Property<string>("RequestID")
                        .IsRequired();

                    b.Property<string>("SMessage");

                    b.Property<string>("Status");

                    b.Property<string>("TTransId");

                    b.Property<string>("TeleSecurityCode");

                    b.Property<string>("Wallet");

                    b.HasKey("Id");

                    b.ToTable("MobileMoney");
                });

            modelBuilder.Entity("Uba.Data.Entity.Remittance", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address");

                    b.Property<string>("BenefBank");

                    b.Property<string>("BenefBranch");

                    b.Property<string>("BenefCode");

                    b.Property<string>("BenefName");

                    b.Property<string>("BenefSortCode");

                    b.Property<string>("BeneficiaryIban");

                    b.Property<string>("ChannelCode");

                    b.Property<string>("ClientId");

                    b.Property<string>("ClientName");

                    b.Property<string>("ErrorCode");

                    b.Property<string>("ErrorDescription");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("SourceCode");

                    b.Property<string>("SourceIban");

                    b.Property<string>("Stan");

                    b.Property<string>("UBATransactionId");

                    b.Property<string>("accountName");

                    b.Property<string>("amount");

                    b.Property<string>("balanceCurrency");

                    b.Property<string>("currency");

                    b.Property<string>("description");

                    b.Property<string>("destinationUri");

                    b.Property<string>("fieldId");

                    b.Property<string>("grace");

                    b.Property<string>("hstransactionId");

                    b.Property<string>("label");

                    b.Property<string>("login");

                    b.Property<DateTime>("maxCompletionDate");

                    b.Property<string>("reportStatusTo");

                    b.Property<string>("responseCode");

                    b.Property<string>("responseMessage");

                    b.Property<string>("routingTag");

                    b.Property<string>("sourceUri");

                    b.Property<string>("state");

                    b.Property<string>("token");

                    b.Property<string>("transactionId");

                    b.Property<string>("validity");

                    b.Property<string>("value");

                    b.HasKey("Id");

                    b.ToTable("Remittance");
                });
#pragma warning restore 612, 618
        }
    }
}
