﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Uba.Migrations
{
    public partial class UpdateMaxCompletionDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "maxCompletionDate",
                table: "Remittance",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "maxCompletionDate",
                table: "Remittance",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
