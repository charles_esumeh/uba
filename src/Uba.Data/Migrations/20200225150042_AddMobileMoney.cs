﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Uba.Migrations
{
    public partial class AddMobileMoney : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MobileMoney",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountNumber = table.Column<string>(nullable: true),
                    Provider = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    TeleSecurityCode = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MSISDN = table.Column<string>(nullable: true),
                    Account_Status = table.Column<string>(nullable: true),
                    Wallet = table.Column<string>(nullable: true),
                    DebitAccount = table.Column<string>(nullable: true),
                    CurrencyCode = table.Column<string>(nullable: true),
                    DigitCurrency = table.Column<string>(nullable: true),
                    RequestID = table.Column<string>(nullable: true),
                    Amount = table.Column<string>(nullable: true),
                    Narration = table.Column<string>(nullable: true),
                    TTransId = table.Column<string>(nullable: true),
                    SMessage = table.Column<string>(nullable: true),
                    Available = table.Column<string>(nullable: true),
                    Ledger = table.Column<string>(nullable: true),
                    FormalRequestID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MobileMoney", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MobileMoney");
        }
    }
}
