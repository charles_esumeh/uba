﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Uba.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MobileMoney",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    RecordStatus = table.Column<int>(nullable: false),
                    VigipayReference = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: false),
                    Provider = table.Column<string>(nullable: false),
                    CountryCode = table.Column<string>(nullable: false),
                    TeleSecurityCode = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    MSISDN = table.Column<string>(nullable: true),
                    Account_Status = table.Column<string>(nullable: true),
                    Wallet = table.Column<string>(nullable: true),
                    DebitAccount = table.Column<string>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: false),
                    DigitCurrency = table.Column<string>(nullable: false),
                    RequestID = table.Column<string>(nullable: false),
                    Amount = table.Column<string>(nullable: false),
                    Narration = table.Column<string>(nullable: false),
                    TTransId = table.Column<string>(nullable: true),
                    SMessage = table.Column<string>(nullable: true),
                    Available = table.Column<string>(nullable: true),
                    Ledger = table.Column<string>(nullable: true),
                    FormalRequestID = table.Column<string>(nullable: false),
                    AccountType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MobileMoney", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Remittance",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    RecordStatus = table.Column<int>(nullable: false),
                    VigipayReference = table.Column<string>(nullable: true),
                    ClientId = table.Column<string>(nullable: true),
                    login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    transactionId = table.Column<string>(nullable: true),
                    UBATransactionId = table.Column<string>(nullable: true),
                    hstransactionId = table.Column<string>(nullable: true),
                    sourceUri = table.Column<string>(nullable: true),
                    destinationUri = table.Column<string>(nullable: true),
                    Stan = table.Column<string>(nullable: true),
                    currency = table.Column<string>(nullable: true),
                    amount = table.Column<string>(nullable: true),
                    grace = table.Column<string>(nullable: true),
                    validity = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    SourceIban = table.Column<string>(nullable: true),
                    SourceCode = table.Column<string>(nullable: true),
                    routingTag = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    ChannelCode = table.Column<string>(nullable: true),
                    fieldId = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true),
                    BeneficiaryIban = table.Column<string>(nullable: true),
                    BenefName = table.Column<string>(nullable: true),
                    BenefBank = table.Column<string>(nullable: true),
                    BenefBranch = table.Column<string>(nullable: true),
                    BenefSortCode = table.Column<string>(nullable: true),
                    BenefCode = table.Column<string>(nullable: true),
                    reportStatusTo = table.Column<string>(nullable: true),
                    token = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    maxCompletionDate = table.Column<DateTime>(nullable: true),
                    state = table.Column<string>(nullable: true),
                    label = table.Column<string>(nullable: true),
                    responseCode = table.Column<string>(nullable: true),
                    responseMessage = table.Column<string>(nullable: true),
                    balanceCurrency = table.Column<string>(nullable: true),
                    accountName = table.Column<string>(nullable: true),
                    ErrorCode = table.Column<string>(nullable: true),
                    ErrorDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Remittance", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MobileMoney");

            migrationBuilder.DropTable(
                name: "Remittance");
        }
    }
}
