﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Uba.Migrations
{
    public partial class UpdateRemittance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ErrorCode",
                table: "Remittance",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ErrorDescription",
                table: "Remittance",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ErrorCode",
                table: "Remittance");

            migrationBuilder.DropColumn(
                name: "ErrorDescription",
                table: "Remittance");
        }
    }
}
