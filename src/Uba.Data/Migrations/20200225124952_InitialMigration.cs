﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Uba.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Remittance",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<string>(nullable: true),
                    login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    transactionId = table.Column<string>(nullable: true),
                    UBATransactionId = table.Column<string>(nullable: true),
                    hstransactionId = table.Column<string>(nullable: true),
                    sourceUri = table.Column<string>(nullable: true),
                    destinationUri = table.Column<string>(nullable: true),
                    Stan = table.Column<string>(nullable: true),
                    currency = table.Column<string>(nullable: true),
                    amount = table.Column<string>(nullable: true),
                    grace = table.Column<string>(nullable: true),
                    validity = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    SourceIban = table.Column<string>(nullable: true),
                    SourceCode = table.Column<string>(nullable: true),
                    routingTag = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    ChannelCode = table.Column<string>(nullable: true),
                    fieldId = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true),
                    BeneficiaryIban = table.Column<string>(nullable: true),
                    BenefName = table.Column<string>(nullable: true),
                    BenefBank = table.Column<string>(nullable: true),
                    BenefBranch = table.Column<string>(nullable: true),
                    BenefSortCode = table.Column<string>(nullable: true),
                    BenefCode = table.Column<string>(nullable: true),
                    reportStatusTo = table.Column<string>(nullable: true),
                    token = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    maxCompletionDate = table.Column<DateTime>(nullable: false),
                    state = table.Column<string>(nullable: true),
                    label = table.Column<string>(nullable: true),
                    responseCode = table.Column<string>(nullable: true),
                    responseMessage = table.Column<string>(nullable: true),
                    balanceCurrency = table.Column<string>(nullable: true),
                    accountName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Remittance", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Remittance");
        }
    }
}
