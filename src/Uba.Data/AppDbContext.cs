﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Uba;
using Uba.Core.Models;
using Uba.Data.Entity;

namespace Uba
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Remittance> Accounts { get; set; }
        public DbSet<Data.Entity.MobileMoney> Wallets { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Remittance>().ToTable("Remittance");
            modelBuilder.Entity<MobileMoney>().ToTable("MobileMoney");
        }
    }
}
